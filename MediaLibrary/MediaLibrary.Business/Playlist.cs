﻿using System;
using System.Collections;
using System.Collections.Generic;
using MediaLibrary.Business.Contracts;

namespace MediaLibrary.Business
{
    public class Playlist : IPlaylist
    {
        private readonly IList<IMediaFile> _mediaFiles;

        public Playlist(string name)
        {
            if (!ValidateName(name))
                throw new ArgumentException(nameof(name));

            _mediaFiles = new List<IMediaFile>();

            Id = Guid.NewGuid();
            Name = name;
        }

        public Playlist(string name, IList<IMediaFile> mediaFiles)
        {
            if (!ValidateName(name))
                throw new ArgumentException(nameof(name));

            _mediaFiles = mediaFiles ?? new List<IMediaFile>();

            Id = Guid.NewGuid();
            Name = name;
        }

        public Guid Id { get; }
        public string Name { get; private set; }

        public int Count => _mediaFiles.Count;

        public IMediaFile this[int index]
        {
            get => _mediaFiles[index];
            private set => _mediaFiles[index] = value;
        }

        public void AddMediaFile(IMediaFile mediaFile)
        {
            _mediaFiles.Add(mediaFile);
        }

        public bool RemoveMediaFile(IMediaFile mediaFile)
        {
            return _mediaFiles.Remove(mediaFile);
        }

        #region Private Methods

        private bool ValidateName(string name)
        {
            var isValid = !string.IsNullOrWhiteSpace(name);
            return isValid;
        }

        #endregion

        #region IEnumerable<T>

        public IEnumerator<IMediaFile> GetEnumerator()
        {
            return _mediaFiles.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}