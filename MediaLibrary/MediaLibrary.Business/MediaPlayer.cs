﻿using System.Collections.Generic;
using MediaLibrary.Business.Contracts;

namespace MediaLibrary.Business
{
    public class MediaPlayer: IMediaPlayer
    {
        public void Play(IEnumerable<IMediaFile> mediaFiles)
        {
            foreach (var mediaFile in mediaFiles)
            {
                var player = mediaFile.GetPlayer();
                player.Play();
            }
        }
    }
}