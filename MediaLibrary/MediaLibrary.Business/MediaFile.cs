﻿using System;
using System.IO;
using MediaLibrary.Business.Contracts;

namespace MediaLibrary.Business
{
    public abstract class MediaFile : IMediaFile
    {
        protected MediaFile(string filePath)
        {
            Id = Guid.NewGuid();
            OriginalFilePath = filePath;
            Name = Path.GetFileNameWithoutExtension(filePath);
        }

        public Guid Id { get; }
        public string Name { get; }

        public string OriginalFilePath { get; }

        public bool IsContentLoaded { get; private set; }
        public byte[] Content { get; private set; }

        public virtual bool LoadContent()
        {
            try
            {
                Content = File.ReadAllBytes(OriginalFilePath);
                IsContentLoaded = true;
            }
            catch (Exception)
            {
                IsContentLoaded = false;
                throw;
            }

            return IsContentLoaded;
        }

        public abstract IPlayer GetPlayer();
    }
}