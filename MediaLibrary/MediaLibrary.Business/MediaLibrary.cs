﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MediaLibrary.Business.Contracts;

namespace MediaLibrary.Business
{
    public class MediaLibrary : IMediaLibrary
    {
        private readonly IMediaPlayer _mediaPlayer;
        
        private readonly IList<IMediaFile> _mediaFiles;
        private readonly IList<Playlist> _playlists;

        public MediaLibrary(IMediaPlayer mediaPlayer)
        {
            _mediaPlayer = mediaPlayer ?? throw new ArgumentNullException(nameof(mediaPlayer));

            _playlists = new List<Playlist>();
            _mediaFiles = new List<IMediaFile>();
        }

        public IPlaylist GetPlaylist(Guid id)
        {
            return _playlists.FirstOrDefault(e => e.Id == id);
        }

        public IPlaylist GetPlaylist(string name)
        {
            return _playlists.FirstOrDefault(e => string.Equals(e.Name, name, StringComparison.OrdinalIgnoreCase));
        }

        public IEnumerable<IMediaFile> SearchMediaFiles(Guid id)
        {
            return this.Where(e => e.Id == id);
        }

        public IEnumerable<IMediaFile> SearchMediaFiles(string name)
        {
            return this.Where(e => e.Name == name);
        }

        public IEnumerable<IMediaFile> SearchMediaFiles(Func<IMediaFile, bool> searchFunc)
        {
            return this.Where(searchFunc);
        }

        public void Play()
        {
            _mediaPlayer.Play(this);
        }

        public void Play(IMediaFile mediaFile)
        {
            _mediaPlayer.Play(new List<IMediaFile> {mediaFile});
        }

        public void Play(IPlaylist playlist)
        {
            _mediaPlayer.Play(playlist);
        }
        
        public int Count => _mediaFiles.Count + _playlists.Sum(e => e.Count);

        public IMediaFile this[int index] => _mediaFiles[index];

        public void AddMediaFile(IMediaFile mediaFile)
        {
            _mediaFiles.Add(mediaFile);
        }

        public bool RemoveMediaFile(IMediaFile mediaFile)
        {
            return _mediaFiles.Remove(mediaFile);
        }

        public IEnumerator<IMediaFile> GetEnumerator()
        {
            foreach (var mediaFile in _mediaFiles) yield return mediaFile;

            foreach (var playlist in _playlists)
            foreach (var mediaFile in playlist)
                yield return mediaFile;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}