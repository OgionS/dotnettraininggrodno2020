﻿using System;
using MediaLibrary.Business.Contracts;
using MediaLibrary.Business.Contracts.MediaFiles;

namespace MediaLibrary.Business.MediaPlayers
{
    public class VideoPlayer : IPlayer
    {
        private readonly IVideoFile _videoFile;


        public VideoPlayer(IVideoFile videoFile)
        {
            _videoFile = videoFile;
        }

        public void Play()
        {
            Console.WriteLine($"Video file '{_videoFile.Name}' is playing...");
        }
    }
}