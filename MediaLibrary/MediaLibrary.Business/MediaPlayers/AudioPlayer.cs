﻿using System;
using MediaLibrary.Business.Contracts;
using MediaLibrary.Business.Contracts.MediaFiles;

namespace MediaLibrary.Business.MediaPlayers
{
    public class AudioPlayer: IPlayer
    {
        private readonly IAudioFile _audioFile;


        public AudioPlayer(IAudioFile audioFile)
        {
            _audioFile = audioFile;
        }

        public void Play()
        {
            Console.WriteLine($"Audio file '{_audioFile.Name}' is playing...");
        }
    }
}