﻿using System;
using MediaLibrary.Business.Contracts;
using MediaLibrary.Business.Contracts.MediaFiles;

namespace MediaLibrary.Business.MediaPlayers
{
    public class PhotoPlayer : IPlayer
    {
        private readonly IPhotoFile _photoFile;


        public PhotoPlayer(IPhotoFile photoFile)
        {
            _photoFile = photoFile;
        }

        public void Play()
        {
            Console.WriteLine($"Photo file '{_photoFile.Name}' is playing...");
        }
    }
}