﻿using System;
using MediaLibrary.Business.Contracts;
using MediaLibrary.Business.Contracts.Enums;
using MediaLibrary.Business.Contracts.MediaFiles;
using MediaLibrary.Business.MediaPlayers;

namespace MediaLibrary.Business.MediaFiles
{
    public class VideoFile : MediaFile, IVideoFile
    {
        public VideoFile(string filePath) : base(filePath)
        {
        }

        public VideoFormat Format { get; private set; }
        public TimeSpan Duration { get; private set; }

        public override bool LoadContent()
        {
            var isContentLoaded = base.LoadContent();

            SetVideoFileAttributes();

            return isContentLoaded;
        }

        public override IPlayer GetPlayer()
        {
            return new VideoPlayer(this);
        }

        private void SetVideoFileAttributes()
        {
            var formatRandom = new Random();
            var durationRandom = new Random();

            Format = (VideoFormat) formatRandom.Next((int) VideoFormat.VideoFormat1, (int) VideoFormat.VideoFormat3);
            Duration = TimeSpan.FromHours(durationRandom.Next(0, 7200));
        }
    }
}