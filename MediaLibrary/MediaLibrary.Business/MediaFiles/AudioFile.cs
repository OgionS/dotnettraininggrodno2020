﻿using System;
using MediaLibrary.Business.Contracts;
using MediaLibrary.Business.Contracts.Enums;
using MediaLibrary.Business.Contracts.MediaFiles;
using MediaLibrary.Business.MediaPlayers;

namespace MediaLibrary.Business.MediaFiles
{
    public class AudioFile : MediaFile, IAudioFile
    {
        public AudioFile(string filePath) : base(filePath)
        {
        }

        public AudioFormat Format { get; private set; }
        public TimeSpan Duration { get; private set; }

        public override bool LoadContent()
        {
            var isContentLoaded = base.LoadContent();

            SetVideoFileAttributes();

            return isContentLoaded;
        }

        public override IPlayer GetPlayer()
        {
            return new AudioPlayer(this);
        }

        private void SetVideoFileAttributes()
        {
            var formatRandom = new Random();
            var durationRandom = new Random();

            Format = (AudioFormat) formatRandom.Next((int) AudioFormat.AudioFormat1, (int) AudioFormat.AudioFormat3);
            Duration = TimeSpan.FromHours(durationRandom.Next(0, 7200));
        }
    }
}