﻿using System;
using MediaLibrary.Business.Contracts;
using MediaLibrary.Business.Contracts.Enums;
using MediaLibrary.Business.Contracts.MediaFiles;
using MediaLibrary.Business.MediaPlayers;

namespace MediaLibrary.Business.MediaFiles
{
    public class PhotoFile : MediaFile, IPhotoFile
    {
        public PhotoFile(string filePath) : base(filePath)
        {
        }

        public PhotoFormat Format { get; private set; }
        public int Width { get; private set; }
        public int Height { get; private set; }

        public override bool LoadContent()
        {
            var isContentLoaded = base.LoadContent();

            SetVideoFileAttributes();

            return isContentLoaded;
        }

        public override IPlayer GetPlayer()
        {
            return new PhotoPlayer(this);
        }

        private void SetVideoFileAttributes()
        {
            var formatRandom = new Random();
            var dimensionsRandom = new Random();

            Format = (PhotoFormat) formatRandom.Next((int) PhotoFormat.PhotoFormat1, (int) PhotoFormat.PhotoFormat3);
            Width = Height = dimensionsRandom.Next(0, 4096);
        }
    }
}