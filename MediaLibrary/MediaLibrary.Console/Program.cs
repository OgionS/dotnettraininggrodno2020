﻿using System.Collections.Generic;
using MediaLibrary.Business;
using MediaLibrary.Business.Contracts;
using MediaLibrary.Business.MediaFiles;

namespace MediaLibrary.Console
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            // Test Data - initialization.
            AudioFile audioFile1 = new AudioFile(@"Audio\Beep.wav");
            AudioFile audioFile2 = new AudioFile(@"Audio\Song.mp3");

            PhotoFile photoFile1 = new PhotoFile(@"Audio\Sun.jpg");
            PhotoFile photoFile2 = new PhotoFile(@"Audio\Rock.bmp");
            PhotoFile photoFile3 = new PhotoFile(@"Audio\Ocean.jpeg");

            VideoFile videoFile1 = new VideoFile(@"Audio\Sun.mkv");
            VideoFile videoFile2 = new VideoFile(@"Audio\Rock.avi");
            VideoFile videoFile3 = new VideoFile(@"Audio\Ocean.mpeg");
            
            var playlistAudio = new Playlist("Playlist Audio", new List<IMediaFile> {audioFile1, audioFile2 });
            playlistAudio.RemoveMediaFile(audioFile1);

            var playlistPhoto = new Playlist("Playlist Photo");
            playlistPhoto.AddMediaFile(photoFile1);
            playlistPhoto.AddMediaFile(photoFile2);
            playlistPhoto.AddMediaFile(photoFile3);

            var playlistVideo = new Playlist("Playlist Video");
            playlistVideo.AddMediaFile(videoFile1);
            playlistVideo.AddMediaFile(videoFile2);
            playlistVideo.AddMediaFile(videoFile3);

            // Media library - files manipulation.
            IMediaPlayer mediaPlayer = new MediaPlayer();
            IMediaLibrary mediaLibrary = new Business.MediaLibrary(mediaPlayer);

            mediaLibrary.AddMediaFile(audioFile1);
            mediaLibrary.AddMediaFile(photoFile1);
            mediaLibrary.AddMediaFile(videoFile1);
            mediaLibrary.RemoveMediaFile(photoFile1);

            IPlaylist videoPlaylist = mediaLibrary.GetPlaylist("Playlist Video");
            videoPlaylist.RemoveMediaFile(videoFile2);
            videoPlaylist.AddMediaFile(videoFile2);

            // Media library - search files.
            var mediaFiles1 = mediaLibrary.SearchMediaFiles(videoFile1.Id);
            var mediaFiles2 = mediaLibrary.SearchMediaFiles(videoFile1.Name);
            var mediaFiles3 = mediaLibrary.SearchMediaFiles(e => e.Name.StartsWith("prefix"));

            // Media library - play files.
            mediaLibrary.Play();
            mediaLibrary.Play(videoFile1);
            mediaLibrary.Play(playlistVideo);
        }
    }
}