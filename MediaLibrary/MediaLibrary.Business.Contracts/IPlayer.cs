﻿namespace MediaLibrary.Business.Contracts
{
    public interface IPlayer
    {
        void Play();
    }
}