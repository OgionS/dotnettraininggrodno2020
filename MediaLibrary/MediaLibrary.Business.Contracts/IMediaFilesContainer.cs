﻿using System.Collections.Generic;

namespace MediaLibrary.Business.Contracts
{
    public interface IMediaFilesContainer: IEnumerable<IMediaFile>
    {
        int Count { get; }

        IMediaFile this[int index] { get; }

        void AddMediaFile(IMediaFile mediaFile);
        bool RemoveMediaFile(IMediaFile mediaFile);
    }
}