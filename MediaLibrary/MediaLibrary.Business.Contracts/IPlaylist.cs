﻿namespace MediaLibrary.Business.Contracts
{
    public interface IPlaylist : IIdentifiable, INameable, IMediaFilesContainer
    {
    }
}