﻿namespace MediaLibrary.Business.Contracts.Enums
{
    public enum AudioFormat
    {
        AudioFormat1 = 0,
        AudioFormat2,
        AudioFormat3,
    }
}