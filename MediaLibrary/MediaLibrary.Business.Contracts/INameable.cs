﻿namespace MediaLibrary.Business.Contracts
{
    public interface INameable
    {
        string Name { get; }
    }
}