﻿namespace MediaLibrary.Business.Contracts
{
    public interface IPlayerProvider
    {
        IPlayer GetPlayer();
    }
}