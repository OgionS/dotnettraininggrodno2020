﻿using System.Collections.Generic;

namespace MediaLibrary.Business.Contracts
{
    public interface IMediaPlayer
    {
        void Play(IEnumerable<IMediaFile> mediaFiles);
    }
}