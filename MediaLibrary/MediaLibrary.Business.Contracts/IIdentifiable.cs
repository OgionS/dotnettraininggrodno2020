﻿using System;

namespace MediaLibrary.Business.Contracts
{
    public interface IIdentifiable
    {
        Guid Id { get; }
    }
}