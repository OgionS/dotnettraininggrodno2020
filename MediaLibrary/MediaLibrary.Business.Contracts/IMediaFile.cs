﻿namespace MediaLibrary.Business.Contracts
{
    public interface IMediaFile : IPlayerProvider, IIdentifiable, INameable
    {
        string OriginalFilePath { get; }
        bool IsContentLoaded { get; }
        byte[] Content { get; }

        bool LoadContent();
    }
}