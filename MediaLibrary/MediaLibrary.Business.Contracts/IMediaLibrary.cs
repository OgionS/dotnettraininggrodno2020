﻿using System;
using System.Collections.Generic;

namespace MediaLibrary.Business.Contracts
{
    public interface IMediaLibrary : IMediaFilesContainer
    {
        IPlaylist GetPlaylist(Guid id);
        IPlaylist GetPlaylist(string name);

        IEnumerable<IMediaFile> SearchMediaFiles(Guid id);
        IEnumerable<IMediaFile> SearchMediaFiles(string name);
        IEnumerable<IMediaFile> SearchMediaFiles(Func<IMediaFile, bool> searchFunc);

        void Play();
        void Play(IMediaFile mediaFile);
        void Play(IPlaylist playlist);
    }
}