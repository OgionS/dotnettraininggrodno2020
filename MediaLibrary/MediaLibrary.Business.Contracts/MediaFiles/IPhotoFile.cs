﻿using MediaLibrary.Business.Contracts.Enums;

namespace MediaLibrary.Business.Contracts.MediaFiles
{
    public interface IPhotoFile : IMediaFile
    {
        PhotoFormat Format { get; }
        int Width { get; }
        int Height { get; }
    }
}