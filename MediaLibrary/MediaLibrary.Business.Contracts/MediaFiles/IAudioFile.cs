﻿using System;
using MediaLibrary.Business.Contracts.Enums;

namespace MediaLibrary.Business.Contracts.MediaFiles
{
    public interface IAudioFile : IMediaFile
    {
        AudioFormat Format { get; }
        TimeSpan Duration { get; }
    }
}