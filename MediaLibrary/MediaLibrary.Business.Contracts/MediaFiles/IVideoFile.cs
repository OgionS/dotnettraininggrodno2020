﻿using System;
using MediaLibrary.Business.Contracts.Enums;

namespace MediaLibrary.Business.Contracts.MediaFiles
{
    public interface IVideoFile : IMediaFile
    {
        VideoFormat Format { get; }
        TimeSpan Duration { get; }
    }
}