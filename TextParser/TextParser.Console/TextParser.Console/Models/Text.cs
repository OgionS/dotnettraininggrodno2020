﻿using System.Collections.Generic;
using TextParser.Models.Contracts;

namespace TextParser.Models
{
    public class Text 
    {
        public ICollection<ISentence> Sentences { get; set; }

        public Text()
        {
            Sentences = new List<ISentence>();
        }
    }
}
