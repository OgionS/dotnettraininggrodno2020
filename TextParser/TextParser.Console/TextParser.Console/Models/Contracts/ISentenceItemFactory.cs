﻿namespace TextParser.Models.Contracts
{
    public interface ISentenceItemFactory
    {
        ISentenceItem Create(string chars);
    }
}
