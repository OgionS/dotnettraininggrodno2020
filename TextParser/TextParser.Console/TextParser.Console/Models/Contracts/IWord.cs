﻿using System.Collections.Generic;

namespace TextParser.Models.Contracts
{
    public interface IWord: ISentenceItem, IEnumerable<Symbol>
    {
        Symbol this[int index] { get; }
        int Length { get; }
    }
}
