﻿namespace TextParser.Models.Contracts
{
    public interface ISentenceItem
    {
        string Chars { get; }
    }
}
