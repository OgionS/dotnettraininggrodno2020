﻿using System.Collections.Generic;

namespace TextParser.Models.Contracts
{
    public interface ISentence : IEnumerable<ISentenceItem>
    {
        void Add(ISentenceItem item);
        bool Remove(ISentenceItem item);
        int Count { get; }
    }
}
