﻿namespace TextParser.Models.Contracts
{
    public interface IPunctuation : ISentenceItem
    {
        Symbol Value { get; }
    }
}
