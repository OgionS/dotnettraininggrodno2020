﻿using TextParser.Models.Contracts;

namespace TextParser.Models
{
    public class WordFactory : ISentenceItemFactory
    {
        public ISentenceItem Create(string chars)
        {
            return new Word(chars);
        }
    }
}
