﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using TextParser.Models;

namespace TextParser
{
    class Program
    {
        static void Main(string[] args)
        {
            const string inputFile = @"TestData\voyna-i-mir-utf.txt";

            // var symbolSize = sizeof(Symbol1);
            // int symbolSize = Marshal.SizeOf(typeof(Symbol));
            int symbolSize1 = Marshal.SizeOf(typeof(Symbol1));

            List<Symbol> symbols = new List<Symbol>();


            using (var sr = File.OpenText(inputFile))
            {
                var text = sr.ReadToEnd();

                var timeout = TimeSpan.FromSeconds(2).TotalMilliseconds;
                while (timeout > 0)
                {
                    timeout = timeout - 200;
                    Thread.Sleep(200);
                }

                foreach (var @char in text)
                {
                    // symbols.Add(@char.ToString());
                    symbols.Add(new Symbol(@char));
                    // symbols.Add(new Symbol1(@char));
                    // symbols.Add(new Symbol1(@char));
                }

                // for (int i = 0; i < 10000000; i++)
                // {
                //     symbols.Add('a');
                // }
            }

            Console.WriteLine(symbols.Count);

            Console.ReadKey();
        }
    }
}
