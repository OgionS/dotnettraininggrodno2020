﻿using TelephoneExchange.Business.Enums;

namespace TelephoneExchange.Business
{
    public class Port
    {
        public Port(int id)
        {
            Id = id;
            State = PortStates.Inactive;
        }

        public int Id { get; }
        public PortStates State { get; set; }
    }
}