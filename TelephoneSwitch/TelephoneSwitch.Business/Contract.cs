﻿using TelephoneExchange.Business.Enums;

namespace TelephoneExchange.Business
{
    public class Contract
    {
        public Customer Customer{ get; set; }
        public TariffTypes TariffType { get; set; }
        public ulong PhoneNumber{ get; set; }
    }
}