﻿using System.Collections.Generic;
using TelephoneExchange.Business.Enums;

namespace TelephoneExchange.Business
{
    public class BillingSystem
    {
        private readonly PhoneNumbersManager _phoneNumbersManager;

        private readonly IList<Contract> _contracts;

        public BillingSystem(PhoneNumbersManager phoneNumbersManager)
        {
            _phoneNumbersManager = phoneNumbersManager;

            _contracts = new List<Contract>();
        }

        public Contract SignContractWithCustomer(Customer customer, TariffTypes tariffType)
        {
            var contract = new Contract
            {
                Customer = customer,
                TariffType = tariffType,
                PhoneNumber = _phoneNumbersManager.GetAvailablePhoneNumber()
            };

            _contracts.Add(contract);

            return contract;
        }
    }
}