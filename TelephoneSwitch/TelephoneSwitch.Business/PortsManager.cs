﻿using System.Collections.Generic;
using System.Linq;
using TelephoneExchange.Business.Enums;

namespace TelephoneExchange.Business
{
    public class PortsManager
    {
        public List<Port> Ports { get; }

        public PortsManager()
        {
            Ports = GetAvaialblePorts().ToList();
        }

        public Port GetFirstInactivePort()
        {
            return Ports.FirstOrDefault(e => e.State == PortStates.Inactive);
        }

        private IEnumerable<Port> GetAvaialblePorts()
        {
            return Enumerable.Range(0, 3).Select(e => new Port(e));
        }
    }
}