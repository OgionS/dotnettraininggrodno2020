﻿namespace TelephoneExchange.Business.Enums
{
    public enum TerminalState
    {
        Attached,
        Deattached
    }
}