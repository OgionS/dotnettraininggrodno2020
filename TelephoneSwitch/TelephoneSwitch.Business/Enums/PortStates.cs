﻿namespace TelephoneExchange.Business.Enums
{
    public enum PortStates
    {
        Inactive = 0,
        Linked,
        Busy,
    }
}