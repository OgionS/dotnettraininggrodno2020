﻿using System;
using TelephoneExchange.Business.Enums;

namespace TelephoneExchange.Business
{
    public class TerminalInfo
    {
        public TerminalInfo(Guid serialNumber, ulong phoneNumber, TerminalState state)
        {
            SerialNumber = serialNumber;
            PhoneNumber = phoneNumber;
            State = state;
        }

        public Guid SerialNumber { get; }
        public ulong PhoneNumber { get; }
        public TerminalState State { get; }
    }
}