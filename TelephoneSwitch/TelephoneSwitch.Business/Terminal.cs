﻿using System;
using TelephoneExchange.Business.Enums;
using TelephoneExchange.Business.EventArguments;

namespace TelephoneExchange.Business
{
    public class Terminal
    {
        private readonly TerminalInfo _terminalInfo;

        public event EventHandler<TerminalRequestActionEventArgs> AttachRequested;
        public event EventHandler<TerminalRequestActionEventArgs> DeattachRequested;

        public event EventHandler<EventArgs> CallRequested;

        public Terminal(ulong phoneNumber)
        {
            SerialNumber = Guid.NewGuid();
            PhoneNumber = phoneNumber;
            State = TerminalState.Deattached;

            _terminalInfo = new TerminalInfo(SerialNumber, PhoneNumber, State);
        }

        public Guid SerialNumber { get; }
        public ulong PhoneNumber { get; }
        public TerminalState State { get; private set; }

        public void Attach()
        {
            var requestActionResult = new RequestActionResult();
            var terminalRequestActionEventArgs = new TerminalRequestActionEventArgs(_terminalInfo, requestActionResult);
            OnAttachEvent(terminalRequestActionEventArgs);

            if (terminalRequestActionEventArgs.ActionResult.Result == ActionResult.Success)
            {
                State = TerminalState.Attached;
            }
            else
            {
                terminalRequestActionEventArgs.ActionResult.RaiseExceptionIfNecessary();
            }
        }

        public void Call(ulong phoneNumber)
        {
            CallRequested?.Invoke(this, EventArgs.Empty);
        }
        
        protected virtual void OnAttachEvent(TerminalRequestActionEventArgs terminalRequestActionEventArgs)
        {
            AttachRequested?.Invoke(this, terminalRequestActionEventArgs);
        }
    }
}