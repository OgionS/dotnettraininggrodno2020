﻿using TelephoneExchange.Business.Enums;

namespace TelephoneExchange.Business
{
    public class Tariff
    {
        public TariffTypes Type { get; set; }
    }
}