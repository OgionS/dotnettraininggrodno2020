﻿using System;
using TelephoneExchange.Business.Enums;

namespace TelephoneExchange.Business
{
    public class CallInfoDataRecord
    {
        public ulong SourcePhoneNumber { get; set; }
        public ulong TargetPhoneNumber { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public int Duration { get; set; }
        public int Cost { get; set; }
        public TariffTypes TariffType { get; set; }
    }
}