﻿using System;
using System.Collections.Generic;
using TelephoneExchange.Business.Enums;
using TelephoneExchange.Business.EventArguments;
using TelephoneExchange.Business.Exceptions;

namespace TelephoneExchange.Business
{
    public class TelephoneSwitch
    {
        private readonly PortsManager _portsManager;
        private readonly TerminalsManager _terminalsManager;

        public TelephoneSwitch(PortsManager portsManager, TerminalsManager terminalsManager)
        {
            _portsManager = portsManager;
            _terminalsManager = terminalsManager;

            PortAndTerminalsMap = new Dictionary<Port, Terminal>();
        }

        public IDictionary<Port, Terminal> PortAndTerminalsMap { get; }

        public Terminal AcquireTerminal(Contract contract)
        {
            if (contract == null) throw new ArgumentNullException(nameof(contract));

            Terminal terminal = _terminalsManager.Add(contract.PhoneNumber);
            RegisterTerminal(terminal);
            return terminal;
        }

        private void RegisterTerminal(Terminal terminal)
        {
            terminal.AttachRequested += TerminalOnAttachRequested;
            // terminal.DeattachRequested += TerminalOnAttachRequested;

            terminal.CallRequested += TerminalOnCallRequested;
        }

        private void TerminalOnCallRequested(object sender, EventArgs e)
        {
        }

        private void UnregisterTerminal(Terminal terminal)
        {
            terminal.AttachRequested -= TerminalOnAttachRequested;
            // terminal.DeattachRequested -= TerminalOnAttachRequested;
        }

        private void TerminalOnAttachRequested(object sender, TerminalRequestActionEventArgs terminalRequestActionEventArgs)
        {
            var port = _portsManager.GetFirstInactivePort();
            if(port == null)
            {
                terminalRequestActionEventArgs.ActionResult.SetFailure(new NoAvailablePortException());
            }

            var terminal = _terminalsManager.Get(terminalRequestActionEventArgs.TerminalInfo.PhoneNumber);
            if(terminal == null)
            {
                terminalRequestActionEventArgs.ActionResult.SetFailure(new TerminalNotFoundException());
            }

            Link(port, terminal);

            terminalRequestActionEventArgs.ActionResult.SetSuccess();
        }

        private void Link(Port port, Terminal terminal)
        {
            PortAndTerminalsMap[port] = terminal;
            port.State = PortStates.Linked;
        }
    }
}