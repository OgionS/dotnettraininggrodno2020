﻿using System.Collections.Generic;
using System.Linq;
using TelephoneExchange.Business.Exceptions;

namespace TelephoneExchange.Business
{
    public class TerminalsManager
    {
        public TerminalsManager()
        {
            Terminals = new List<Terminal>();
        }

        public IList<Terminal> Terminals { get; }

        public Terminal Add(ulong phoneNumber)
        {
            if(IsTerminalRegistered(phoneNumber))
                throw new TerminalIsAlreadyRegisteredException();

            var terminal = new Terminal(phoneNumber);
            Terminals.Add(terminal);
            return terminal;
        }

        public Terminal Get(ulong phoneNumber)
        {
            return Terminals.FirstOrDefault(e => e.PhoneNumber == phoneNumber);
        }

        public bool IsTerminalRegistered(ulong phoneNumber)
        {
            return Terminals.Any(e => e.PhoneNumber == phoneNumber);
        }
    }
}