﻿using System;
using TelephoneExchange.Business.Enums;

namespace TelephoneExchange.Business.EventArguments
{
    public class RequestActionResult
    {
        public ActionResult Result { get; set; }
        public Exception Exception { get; set; }

        public void SetFailure(Exception exception)
        {
            Result = ActionResult.Failure;
            Exception = exception;
        }

        public void SetSuccess()
        {
            Result = ActionResult.Success;
            Exception = null;
        }

        public void RaiseExceptionIfNecessary()
        {
            if (Result == ActionResult.Failure) throw Exception ?? new Exception("Unknown excpetion.");
        }
    }
}