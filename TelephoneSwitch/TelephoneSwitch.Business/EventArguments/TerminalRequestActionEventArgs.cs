﻿using System;

namespace TelephoneExchange.Business.EventArguments
{
    public class TerminalRequestActionEventArgs : EventArgs
    {
        public TerminalRequestActionEventArgs(TerminalInfo terminalInfo, RequestActionResult actionResult)
        {
            TerminalInfo = terminalInfo;
            ActionResult = actionResult;
        }

        public TerminalInfo TerminalInfo { get; }
        public RequestActionResult ActionResult { get; }
    }
}