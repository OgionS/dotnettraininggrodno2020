﻿using System;

namespace TelephoneExchange.Business.Exceptions
{
    public class TerminalIsAlreadyRegisteredException : Exception
    {
        public TerminalIsAlreadyRegisteredException() : base("Terminal has been already registered.")
        {
        }
    }
}