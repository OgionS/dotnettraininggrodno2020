﻿using System;

namespace TelephoneExchange.Business.Exceptions
{
    public class NoAvailablePortException : Exception
    {
        public NoAvailablePortException() : base("No available port found.")
        {
        }
    }
}