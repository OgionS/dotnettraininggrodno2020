﻿using System;

namespace TelephoneExchange.Business.Exceptions
{
    public class TerminalNotFoundException : Exception
    {
        public TerminalNotFoundException() : base("Terminal not found.")
        {
        }
    }
}