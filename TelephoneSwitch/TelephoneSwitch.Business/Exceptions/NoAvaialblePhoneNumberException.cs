﻿using System;

namespace TelephoneExchange.Business.Exceptions
{
    public class NoAvaialblePhoneNumberException : Exception
    {
        public NoAvaialblePhoneNumberException() : base("No available phone number.")
        {
        }
    }
}