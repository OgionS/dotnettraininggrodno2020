﻿using System.Collections.Generic;
using System.Linq;
using TelephoneExchange.Business.Exceptions;

namespace TelephoneExchange.Business
{
    public class PhoneNumbersManager
    {
        private readonly HashSet<ulong> _availablePhoneNumbers;
        private readonly HashSet<ulong> _busyPhoneNumbers;

        public PhoneNumbersManager()
        {
            _busyPhoneNumbers = new HashSet<ulong>();
            _availablePhoneNumbers = new HashSet<ulong>
            {
                1111111,
                2222222,
                3333333
            };
        }

        public ulong GetAvailablePhoneNumber()
        {
            if (_availablePhoneNumbers.Any())
            {
                var phoneNumber = _availablePhoneNumbers.First();

                _busyPhoneNumbers.Add(phoneNumber);
                _availablePhoneNumbers.Remove(phoneNumber);

                return phoneNumber;
            }
            else
            {
                throw new NoAvaialblePhoneNumberException();
            }
        }
    }
}