﻿namespace TelephoneExchange.Common.Enums
{
    public enum LogLevel
    {
        Off = 0,
        Error,
        Warning,
        Info,
    }
}