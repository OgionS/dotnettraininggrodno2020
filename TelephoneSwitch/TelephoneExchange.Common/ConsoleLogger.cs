﻿using System;
using TelephoneExchange.Common.Contracts;
using TelephoneExchange.Common.Enums;

namespace TelephoneExchange.Common
{
    public class ConsoleLogger : ILogger
    {
        private readonly LogLevel _logLevel;

        public ConsoleLogger(LogLevel logLevel)
        {
            _logLevel = logLevel;
        }

        public void Error(string message)
        {
            LogMessage(LogLevel.Error, message);
        }

        public void Warn(string message)
        {
            LogMessage(LogLevel.Warning, message);
        }

        public void Info(string message)
        {
            LogMessage(LogLevel.Info, message);
        }

        private void LogMessage(LogLevel msgLogLevel, string message)
        {
            if (_logLevel >= msgLogLevel)
            {
                Console.WriteLine($"{DateTime.Now:HH:mm:ss.fffff} - {msgLogLevel.ToString()[0]}: {message}");
            }
        }
    }
}