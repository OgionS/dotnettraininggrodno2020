﻿using System;
using TelephoneExchange.Business;
using TelephoneExchange.Business.Enums;
using TelephoneExchange.Common;
using TelephoneExchange.Common.Contracts;
using TelephoneExchange.Common.Enums;

namespace TelephoneExchange.Example
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            ILogger logger = new ConsoleLogger(LogLevel.Info);

            var phoneNumbersManager = new PhoneNumbersManager();
            var portsManager = new PortsManager();
            var terminalsManager = new TerminalsManager();
            var telephoneSwitch = new TelephoneSwitch(portsManager, terminalsManager);
            var billingSystem = new BillingSystem(phoneNumbersManager);

            var customerJohn = new Customer("John", "Smith", "john_smith@gmail.com");
            var customerRob = new Customer("Rob", "Brown", "rob_brown@gmail.com");
            var customerMichael = new Customer("Michael", "Williams", "michael_williams@gmail.com");

            var standardTariffType = TariffTypes.Standard;

            Contract contractJohn = billingSystem.SignContractWithCustomer(customerJohn, standardTariffType);
            Contract contractRob = billingSystem.SignContractWithCustomer(customerRob, standardTariffType);
            Contract contractMichael = billingSystem.SignContractWithCustomer(customerMichael, standardTariffType);

            Terminal terminalJohn = telephoneSwitch.AcquireTerminal(contractJohn);
            Terminal terminalRob = telephoneSwitch.AcquireTerminal(contractRob);
            Terminal terminalMichael = telephoneSwitch.AcquireTerminal(contractMichael);

            terminalJohn.Attach();
            terminalRob.Attach();

            terminalJohn.Call(contractRob.PhoneNumber);
            


            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}